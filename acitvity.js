

db.fruits.insertMany([
		{
			"name": "Banana",
			"supplier": "Farmer Fruits Inc",
			"stocks": 30,
			"price": 20,
			"onSale": true
		},
		{
			"name": "Mango",
			"supplier": "Mango Magic Inc",
			"stocks": 50,
			"price": 70,
			"onSale": true
		},
		{
			"name": "Dragon Fruit",
			"supplier": "Farmer Fruits Inc",
			"stocks": 10,
			"price": 60,
			"onSale": true
		},
		{
			"name": "Grapes",
			"supplier": "Fruity Co.",
			"stocks": 30,
			"price": 100,
			"onSale": true
		},
		{
			"name": "Apple",
			"supplier": "Apple Valley",
			"stocks": 0,
			"price": 20,
			"onSale": false
		},
		{
			"name": "Papaya",
			"supplier": "Fruity Co.",
			"stocks": 15,
			"price": 60,
			"onSale": true
		},
	]);

//#2
db.fruits.aggregate([
		{$match: {"onSale": true}},
		{$count: "Fruits_On_Sale"}
	]);

//#3
db.fruits.aggregate([
		{$match: {stocks: {$gt: 20}}},
		{$count: "Fruits_more_than20"}
	]);

//#4
db.fruits.aggregate([
		{$match: {"onSale": true}},
		{$group: {_id: "$supplier", Avg_price: {$avg:
			"$price"
		}}}
	]);

//#5
db.fruits.aggregate([
		{$group: {_id: "$supplier", Max_price: {$max: "$price"}} },
		{$sort: {"Max_price" : 1}}	
	]);

//#6
db.fruits.aggregate([
		{$group: {_id: "$supplier", Min_price: {$min: "$price"}}},
		{$sort: {"Min_price" : 1}}	
	]);
